# MQTT.Chat

MQTT.Chat broker is a fully open source, highly scalable, highly available distributed MQTT messaging broker for IoT,It is based on [MQTTnet](https://github.com/chkr1011/MQTTnet) 


Demo online:  https://mqtt.chat

它是基于 MQTTnet 并集成了X509证书进行双向通讯加密和验证！

1、安全连接，访问控制
     SSL/TLS双向证书认证支持、基本用户认证

2、私有云部署，高可靠低成本

     自建云服务器中部署，代码自主可控成本低，不需要按设备连接或消息数量付费

3、MQTT协议:

        完整MQTT V3.1.1协议
        QoS 0/1/2消息            
        持久会话与离线消息            
        Retained消息            
        遗愿(Last Will)消息

4、数据库任意切换 

     目前支持SQLite、SQLServer、PostgreSQL 消息存储和接入信息管理
